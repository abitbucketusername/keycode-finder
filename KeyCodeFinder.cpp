#include <iostream>

using namespace std;

int keyCode;
char charAtZero, input[255];
int checkString(char x[]);
string checker;
bool running = true;

int main(){
    cout << "Program by/at abitbucketusername" << endl;
    cout << "Finds the key code associated with a key \n(Key Codes tested to work with c++ and Java)" << endl;
    cout << "Arrow keys are Left(72), Right(77), Up(72), Down(80)" << endl;
    cout << "Note: Only finds numbers or letters" << endl;
    cout << "type exit to quit" << endl << endl;
    while(running){
         cout << "Type the key and press enter: ";
         cin.getline(input, 255);
         checker = input;
         if(checker == "exit" || checker == "Exit" || checker == "EXIT")running = false;
         cout << "Key Code: " << checkString(input);
         cout << endl << endl;     
    }
}

int checkString(char x[]){
    charAtZero = x[0];
    switch(toupper(charAtZero)){
        case 'Q':
             keyCode=81;
             break;
        case 'W':
             keyCode=87;
             break;
        case 'E':
             keyCode=69;
             break;
        case 'R':
             keyCode=82;
             break;
        case 'T':
             keyCode=84;
             break;
        case 'Y':
             keyCode=89;
             break;
        case 'U':
             keyCode=85;
             break;
        case 'I':
             keyCode=73;
             break;
        case 'O':
             keyCode=79;
             break;
        case 'P':
             keyCode=80;
             break;
        case 'A':
             keyCode=65;
             break;
        case 'S':
             keyCode=83;
             break;
        case 'D':
             keyCode=68;
             break;
        case 'F':
             keyCode=70;
             break;
        case 'G':
             keyCode=71;
             break;
        case 'H':
             keyCode=72;
             break;
        case 'J':
             keyCode=74;
             break;
        case 'K':
             keyCode=75;
             break;
        case 'L':
             keyCode=76;
             break;
        case 'Z':
             keyCode=90;
             break;                                           
        case 'X':
             keyCode=88;
             break;
        case 'C':
             keyCode=67;
             break;
        case 'V':
             keyCode=86;
             break;
        case 'B':
             keyCode=66;
             break;
        case 'N':
             keyCode=78;
             break;
        case 'M':
             keyCode=77;
             break;
        case '1':
             keyCode=49;
             break;
        case '2':
             keyCode=50;
             break;
        case '3':
             keyCode=51;
             break;
        case '4':
             keyCode=52;
             break;
        case '5':
             keyCode=53;
             break;
        case '6':
             keyCode=54;
             break;                          
        case '7':
             keyCode=55;
             break;
        case '8':
             keyCode=56;
             break;             
        case '9':
             keyCode=57;
             break;
        case '0':
             keyCode=48;
             break;
        default: break;                               
    }
    return keyCode;      
}
